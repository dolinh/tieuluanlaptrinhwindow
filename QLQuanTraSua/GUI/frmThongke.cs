﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmThongke : Form
    {
        public frmThongke()
        {
            InitializeComponent();
            DateTime dtt = DateTime.Now;
            dtpTo.Value = dtpFrom.Value = dtt;
            LoadForm();
        }
        void LoadForm()
        {
            string sql = @"exec thongketheohoadon @ngayBD, @ngayKT";
            DataTable dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text,
                "@ngayBD", SqlDbType.Date, dtpTo.Value,
                "@ngayKT", SqlDbType.Date, dtpFrom.Value);
            dtgvTKTheoHoaDon.DataSource = dt;
            int tienMon = 0, giamgia = 0;
            foreach(DataRow dr in dt.Rows)
            {
                tienMon += Convert.ToInt32(dr["Tiền món"]);
                giamgia += Convert.ToInt32(dr["Tiền món"]) - Convert.ToInt32(dr["Thành tiền"]);
            }
            sql = @"exec thongketheomon @ngayBD, @ngayKT";
            DataTable dt2 = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text,
                "@ngayBD", SqlDbType.Date, dtpTo.Value,
                "@ngayKT", SqlDbType.Date, dtpFrom.Value);
            dtgvTKMon.DataSource = dt2;

            lbTongSoHoaDon.Text = dt.Rows.Count.ToString();
            lbTongSoMonDaBan.Text = dt2.Rows.Count.ToString();
            lbTienMon.Text = tienMon.ToString();
            lbTienGiamGia.Text = giamgia.ToString();
            lbTienThuVe.Text = (tienMon - giamgia).ToString();

            DataTable dt3;
            sql = @"select count(tentaikhoan) as 'sl' from tbl_taikhoan";
            dt3 = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
            lbTongSoTaiKhoan.Text = dt3.Rows[0]["sl"].ToString();

            sql = @"select count(maban) as 'sl' from tbl_ban";
            dt3 = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
            lbTongSoBan.Text = dt3.Rows[0]["sl"].ToString();

            sql = @"select count(mamon) as 'sl' from tbl_thucdon";
            dt3 = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
            lbTongSoMon.Text = dt3.Rows[0]["sl"].ToString();

            sql = @"select count(maloai) as 'sl' from tbl_nhommon";
            dt3 = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
            lbTongSoLoai.Text = dt3.Rows[0]["sl"].ToString();
        }

        private void bntThongKe_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void frmThongke_Activated(object sender, EventArgs e)
        {
            LoadForm();
        }
    }
}
