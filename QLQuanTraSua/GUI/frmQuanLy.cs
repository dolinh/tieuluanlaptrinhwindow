﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmQuanLy : Form
    {
        #region Init
        private int showIndex = 1;
        private int selectRow = 0;
        private string sql;
        private DataTable dt;
        public frmQuanLy()
        {
            InitializeComponent();
            loadForm();
        }

        #endregion

        #region Method
        void loadForm()
        {
            showTab();
        }

        void showTab(string tk = "")
        {
            selectRow = 0;
            dtgv.DataSource = null;
            lbNhomMon.Hide();
            cbbNhomMon.Hide();
            switch (showIndex)
            {
                case 1:
                    lbNhomMon.Show();
                    cbbNhomMon.Show();
                    lbTenButton.Text = "QUẢN LÝ THỰC ĐƠN";
                    lbTim.Text = "Tìm món:";
                    lbTongSo.Text = "Tổng số món:";
                    if (tk == "")
                    {
                        sql = @"select mamon as N'Mã món', tenmon as N'Tên món', maloai as N'Mã loại', dongia as N'Đơn giá', donvitinh as N'Đơn vị tính' from tbl_thucdon";
                    }
                    else
                    {
                        sql = @"select mamon as N'Mã món', tenmon as N'Tên món', maloai as N'Mã loại', dongia as N'Đơn giá', donvitinh as N'Đơn vị tính' from tbl_thucdon where tenmon like '%" + tk + "%'";
                    }
                    dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
                    dtgv.DataSource = dt;
                    lbSL.Text = dt.Rows.Count.ToString();
                    cbbNhomMon.Items.Clear();
                    cbbNhomMon.Items.Add("Tất cả");
                    cbbNhomMon.SelectedIndex = 0;
                    sql = @"select tenloai from tbl_nhommon";
                    DataTable dtt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
                    foreach (DataRow dr in dtt.Rows)
                    {
                        cbbNhomMon.Items.Add(dr["tenloai"]);
                    }
                    break;
                case 2:
                    lbTenButton.Text = "QUẢN LÝ NHÓM MÓN";
                    lbTim.Text = "Tìm nhóm:";
                    lbTongSo.Text = "Tổng số nhóm:";
                    if (tk == "")
                    {
                        sql = @"select maloai as N'Mã nhóm món', tenloai as N'Tên nhóm món' from tbl_nhommon";
                    }
                    else
                    {
                        sql = @"select maloai as N'Mã nhóm món', tenloai as N'Tên nhóm món' from tbl_nhommon where tenloai like '%" + tk + "%'";
                    }
                    dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
                    dtgv.DataSource = dt;
                    lbSL.Text = dt.Rows.Count.ToString();
                    break;
                case 3:
                    lbTenButton.Text = "QUẢN LÝ BÀN";
                    lbTim.Text = "Tìm bàn:";
                    lbTongSo.Text = "Tổng số bàn:";
                    if (tk == "")
                    {
                        sql = @"select maban as N'Mã bàn', tenban as N'Tên bàn', (case when trangthai = 0 then N'Còn trống' when trangthai = 1 then N'Đã đặt trước' else N'Đang phục vụ' end) as N'Trạng thái' from tbl_ban";
                    }
                    else
                    {
                        sql = @"select maban as N'Mã bàn', tenban as N'Tên bàn', (case when trangthai = 0 then N'Còn trống' when trangthai = 1 then N'Đã đặt trước' else N'Đang phục vụ' end) as N'Trạng thái' from tbl_ban where tenban like '%" + tk + "%'";
                    }
                    dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
                    dtgv.DataSource = dt;
                    lbSL.Text = dt.Rows.Count.ToString();
                    break;
                case 4:
                    lbTenButton.Text = "QUẢN LÝ TÀI KHOẢN";
                    lbTim.Text = "Tìm tài khoản:";
                    lbTongSo.Text = "Tổng số tài khoản:";
                    if (tk == "")
                    {
                        sql = @"select tentaikhoan as N'Tên tài khoản', matkhau as N'Mật khẩu', (case when quyen = 0 then N'Quản lý' when quyen = 1 then N'Nhân viên' end) as N'Quyền' from tbl_taikhoan";
                    }
                    else
                    {
                        sql = @"select tentaikhoan as N'Tên tài khoản', matkhau as N'Mật khẩu', (case when quyen = 0 then N'Quản lý' when quyen = 1 then N'Nhân viên' end) as N'Quyền' from tbl_taikhoan where tentaikhoan like '%" + tk + "%'";
                    }
                    dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
                    dtgv.DataSource = dt;
                    lbSL.Text = dt.Rows.Count.ToString();
                    break;
            }
            dtgv.ReadOnly = true;
        }

        void addItem()
        {
            switch (showIndex)
            {
                case 1:
                    frmThucDon fThucDon = new frmThucDon();
                    fThucDon.ShowDialog();
                    break;
                case 2:
                    frmNhomMon fNhomMon = new frmNhomMon();
                    fNhomMon.ShowDialog();
                    break;
                case 3:
                    frmBan fBan = new frmBan();
                    fBan.ShowDialog();
                    break;
                case 4:
                    frmTaiKhoan fTaiKhoan = new frmTaiKhoan();
                    fTaiKhoan.ShowDialog();
                    break;
            }
            showTab();
        }

        void SuaItem()
        {
            switch (showIndex)
            {
                case 1:
                    frmThucDon fThucDon = new frmThucDon(dt.Rows[selectRow]);
                    fThucDon.ShowDialog();
                    break;
                case 2:
                    frmNhomMon fNhomMon = new frmNhomMon(dt.Rows[selectRow]);
                    fNhomMon.ShowDialog();
                    break;
                case 3:
                    frmBan fBan = new frmBan(dt.Rows[selectRow]);
                    fBan.ShowDialog();
                    break;
                case 4:
                    frmTaiKhoan fTaiKhoan = new frmTaiKhoan(dt.Rows[selectRow]);
                    fTaiKhoan.ShowDialog();
                    break;
            }
            showTab();
        }

        void XoaItem()
        {
            if (MessageBox.Show("Bạn có chắc chắn xóa?", "Cảnh báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                switch (showIndex)
                {
                    case 1:
                        sql = @"delete from tbl_thucdon where tenmon = @tenmon";
                        SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text, "@tenmon", SqlDbType.NVarChar, dt.Rows[selectRow]["Tên món"].ToString());
                        break;
                    case 2:
                        sql = @"delete from tbl_nhommon where tenloai = @tenloai";
                        SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text, "@tenloai", SqlDbType.NVarChar, dt.Rows[selectRow]["Tên nhóm món"].ToString());
                        break;
                    case 3:
                        sql = @"delete from tbl_ban where tenban = @tenban";
                        SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text, "@tenban", SqlDbType.NVarChar, dt.Rows[selectRow]["Tên bàn"].ToString());
                        break;
                    case 4:
                        sql = @"delete from tbl_taikhoan where tentaikhoan = @tentaikhoan";
                        SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text, "@tentaikhoan", SqlDbType.NVarChar, dt.Rows[selectRow]["Tên tài khoản"].ToString());
                        break;
                }
            }
            showTab();
        }
        #endregion

        #region Event
        private void bntThem_Click(object sender, EventArgs e)
        {
            addItem();
        }

        private void bntSua_Click(object sender, EventArgs e)
        {
            SuaItem();
        }

        private void bntXoa_Click(object sender, EventArgs e)
        {
            XoaItem();
        }

        private void bntQLThucDon_Click(object sender, EventArgs e)
        {
            showIndex = 1;
            showTab();
        }

        private void bntQLNhomMon_Click(object sender, EventArgs e)
        {
            showIndex = 2;
            showTab();
        }

        private void bntQLBan_Click(object sender, EventArgs e)
        {
            showIndex = 3;
            showTab();
        }

        private void bntQLTaiKhoan_Click(object sender, EventArgs e)
        {
            showIndex = 4;
            showTab();
        }

        private void dtgv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectRow = dtgv.CurrentCell.RowIndex;
        }
        
        private void frmQuanLy_Activated(object sender, EventArgs e)
        {
            showTab();
        }

        private void txtTim_TextChanged(object sender, EventArgs e)
        {
            showTab(txtTim.Text);
        }

        private void cbbNhomMon_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbbNhomMon.SelectedIndex != 0)
            {
                sql = @"select a.mamon as N'Mã món', a.tenmon as N'Tên món', a.maloai as N'Mã loại', a.dongia as N'Đơn giá', a.donvitinh as N'Đơn vị tính' from tbl_thucdon a, tbl_nhommon b where a.maloai = b.maloai and b.tenloai = N'" + cbbNhomMon.Text + "'";
                dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
                dtgv.DataSource = dt;
            }
            else
            {
                sql = @"select mamon as N'Mã món', tenmon as N'Tên món', maloai as N'Mã loại', dongia as N'Đơn giá', donvitinh as N'Đơn vị tính' from tbl_thucdon";
                dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text);
                dtgv.DataSource = dt;
            }
        }
        #endregion
    }
}
