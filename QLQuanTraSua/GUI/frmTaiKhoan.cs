﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmTaiKhoan : Form
    {
        private int loai;
        private DataRow dr;
        public frmTaiKhoan()
        {
            loai = 0;
            InitializeComponent();
            LoadForm();
        }
        public frmTaiKhoan(DataRow _dr)
        {
            loai = 1;
            dr = _dr;
            InitializeComponent();
            LoadForm();
        }

        void LoadForm()
        {
            if (loai == 0)
            {
                lbTen.Text = "Thêm tài khoản";
                cbbQuyen.SelectedIndex = 0;
            }
            else
            {
                lbTen.Text = "Sửa tài khoản";
                txtTenTK.Text = dr["Tên tài khoản"].ToString();
                txtMK.Text = dr["Mật khẩu"].ToString();
                cbbQuyen.SelectedIndex = (dr["Quyền"].ToString() == "Quản lý" ? 0 : 1);
            }
        }

        private void bntHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntXacNhan_Click(object sender, EventArgs e)
        {
            if(loai == 0)
            {
                string sql = @"select * from tbl_taikhoan where tentaikhoan=@tentaikhoan";
                DataTable dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text, "@tentaikhoan", SqlDbType.NVarChar, txtTenTK.Text);
                if(dt.Rows.Count > 0)
                {
                    MessageBox.Show("Tài khoản đã tồn tại", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    sql = @"insert into tbl_taikhoan values(@tentaikhoan,@matkhau,@quyen)";
                    SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                        "@tentaikhoan", SqlDbType.NVarChar, txtTenTK.Text,
                        "@matkhau", SqlDbType.NVarChar, txtMK.Text,
                        "@quyen", SqlDbType.Int, cbbQuyen.SelectedIndex);
                    MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK);
                    this.Close();
                }
            }
            else
            {
                string sql = @"update tbl_taikhoan set tentaikhoan=@tentaikhoan, matkhau=@matkhau, quyen=@quyen where tentaikhoan=@vitri";
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                        "@tentaikhoan", SqlDbType.NVarChar, txtTenTK.Text,
                        "@matkhau", SqlDbType.NVarChar, txtMK.Text,
                        "@quyen", SqlDbType.Int, cbbQuyen.SelectedIndex,
                        "@vitri", SqlDbType.NVarChar, dr["Tên tài khoản"].ToString());
                MessageBox.Show("Thay đổi thành công!", "Thông báo", MessageBoxButtons.OK);
                this.Close();
            }
        }
    }
}
