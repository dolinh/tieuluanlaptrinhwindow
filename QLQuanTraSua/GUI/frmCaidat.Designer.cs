﻿namespace QLQuanTraSua.GUI
{
    partial class frmCaidat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.bntThongtin = new System.Windows.Forms.Button();
            this.pnlThongtin = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlThongtin.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.bntThongtin);
            this.panel1.Location = new System.Drawing.Point(23, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(164, 476);
            this.panel1.TabIndex = 0;
            // 
            // bntThongtin
            // 
            this.bntThongtin.Location = new System.Drawing.Point(16, 13);
            this.bntThongtin.Name = "bntThongtin";
            this.bntThongtin.Size = new System.Drawing.Size(132, 39);
            this.bntThongtin.TabIndex = 0;
            this.bntThongtin.Text = "Thông tin";
            this.bntThongtin.UseVisualStyleBackColor = true;
            this.bntThongtin.Click += new System.EventHandler(this.bntThongtin_Click);
            // 
            // pnlThongtin
            // 
            this.pnlThongtin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlThongtin.Controls.Add(this.label1);
            this.pnlThongtin.Location = new System.Drawing.Point(194, 13);
            this.pnlThongtin.Name = "pnlThongtin";
            this.pnlThongtin.Size = new System.Drawing.Size(824, 475);
            this.pnlThongtin.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Viết thông tin vào đây";
            // 
            // frmCaidat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 500);
            this.Controls.Add(this.pnlThongtin);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(140, 70);
            this.Name = "frmCaidat";
            this.Text = "frmCaidat";
            this.panel1.ResumeLayout(false);
            this.pnlThongtin.ResumeLayout(false);
            this.pnlThongtin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bntThongtin;
        private System.Windows.Forms.Panel pnlThongtin;
        private System.Windows.Forms.Label label1;
    }
}