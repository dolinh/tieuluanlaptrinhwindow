﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmNhomMon : Form
    {
        private int loai;
        private DataRow dr;

        public frmNhomMon()
        {
            loai = 0;
            InitializeComponent();
            LoadForm();
        }

        public frmNhomMon(DataRow _dr)
        {
            loai = 1;
            dr = _dr;
            InitializeComponent();
            LoadForm();
        }
        void LoadForm()
        {
            if (loai == 0)
            {
                lbTen.Text = "Thêm Nhóm Món";
            }
            else
            {
                lbTen.Text = "Sửa Nhóm Món";
                txtTenLoai.Text = dr["Tên nhóm món"].ToString();
            }
        }
        private void bntHuyBo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntXacNhan_Click(object sender, EventArgs e)
        {
            if (loai == 0)
            {
                string sql = @"select * from tbl_nhommon where tenloai=@tenloai";
                DataTable dt = SqlServerHelper.ExecuteDataTable(sql, CommandType.Text, "@tenloai", SqlDbType.NVarChar, txtTenLoai.Text);
                if (dt.Rows.Count > 0)
                {
                    MessageBox.Show("Loại đã tồn tại", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    sql = @"insert into tbl_nhommon(tenloai) values(@loai)";
                    SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                        "@loai", SqlDbType.NVarChar, txtTenLoai.Text);
                    MessageBox.Show("Thêm thành công", "Thông báo", MessageBoxButtons.OK);
                    this.Close();
                }
            }
            else
            {
                string sql = @"update tbl_nhommon set tenloai=@tenloai where tenloai=@vitri";
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                        "@tenloai", SqlDbType.NVarChar, txtTenLoai.Text,
                        "@vitri", SqlDbType.NVarChar, dr["Tên nhóm món"].ToString());
                MessageBox.Show("Thay đổi thành công!", "Thông báo", MessageBoxButtons.OK);
                this.Close();
            }
        }
    }
}
