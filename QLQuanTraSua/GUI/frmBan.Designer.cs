﻿namespace QLQuanTraSua.GUI
{
    partial class frmBan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTen = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenban = new System.Windows.Forms.TextBox();
            this.bntXacNhan = new System.Windows.Forms.Button();
            this.bntHuyBo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbTen
            // 
            this.lbTen.AutoSize = true;
            this.lbTen.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTen.ForeColor = System.Drawing.Color.Red;
            this.lbTen.Location = new System.Drawing.Point(75, 23);
            this.lbTen.Name = "lbTen";
            this.lbTen.Size = new System.Drawing.Size(49, 19);
            this.lbTen.TabIndex = 0;
            this.lbTen.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(34, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên bàn:";
            // 
            // txtTenban
            // 
            this.txtTenban.Location = new System.Drawing.Point(101, 70);
            this.txtTenban.Name = "txtTenban";
            this.txtTenban.Size = new System.Drawing.Size(162, 20);
            this.txtTenban.TabIndex = 0;
            // 
            // bntXacNhan
            // 
            this.bntXacNhan.Location = new System.Drawing.Point(37, 124);
            this.bntXacNhan.Name = "bntXacNhan";
            this.bntXacNhan.Size = new System.Drawing.Size(75, 23);
            this.bntXacNhan.TabIndex = 3;
            this.bntXacNhan.Text = "Xác nhận";
            this.bntXacNhan.UseVisualStyleBackColor = true;
            this.bntXacNhan.Click += new System.EventHandler(this.bntXacNhan_Click);
            // 
            // bntHuyBo
            // 
            this.bntHuyBo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bntHuyBo.Location = new System.Drawing.Point(136, 124);
            this.bntHuyBo.Name = "bntHuyBo";
            this.bntHuyBo.Size = new System.Drawing.Size(75, 23);
            this.bntHuyBo.TabIndex = 4;
            this.bntHuyBo.Text = "Hủy bỏ";
            this.bntHuyBo.UseVisualStyleBackColor = true;
            this.bntHuyBo.Click += new System.EventHandler(this.bntHuyBo_Click);
            // 
            // frmBan
            // 
            this.AcceptButton = this.bntXacNhan;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.bntHuyBo;
            this.ClientSize = new System.Drawing.Size(275, 174);
            this.Controls.Add(this.bntHuyBo);
            this.Controls.Add(this.bntXacNhan);
            this.Controls.Add(this.txtTenban);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbTen);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmBan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TaiKhoan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbTen;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTenban;
        private System.Windows.Forms.Button bntXacNhan;
        private System.Windows.Forms.Button bntHuyBo;
    }
}