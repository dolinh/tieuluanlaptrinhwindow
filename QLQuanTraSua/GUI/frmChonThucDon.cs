﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLQuanTraSua.GUI
{
    public partial class frmChonThucDon : Form
    {
        public frmChonThucDon()
        {
            InitializeComponent();
        }
        string TenBan, mahoadon,table_chitiethoadon = "tbl_chitiethoadon";
        DataRow dr;
        public frmChonThucDon(string _TenBan,string _mahoadon, DataRow _dr)
        {
            InitializeComponent();
            dr = _dr;
            mahoadon = _mahoadon;
            txtSL.Text = "1";
            txtGia.Text = Convert.ToInt32(_dr["dongia"].ToString()).ToString();
            lbTenBan.Text = _TenBan;
            lbTenMon.Text = _dr["tenmon"].ToString();
            lbDvt.Text = _dr["donvitinh"].ToString();
            TenBan = _TenBan;
        }

        private void bntSub_Click(object sender, EventArgs e)
        {
            int SL = Convert.ToInt32(txtSL.Text);
            SL--;
            if (SL < 0)
                SL = 0;
            txtSL.Text = SL.ToString();
        }

        private void bntAdd_Click(object sender, EventArgs e)
        {
            int SL = Convert.ToInt32(txtSL.Text);
            SL++;
            txtSL.Text = SL.ToString();
        }

        private void txtSL_TextChanged(object sender, EventArgs e)
        {
            foreach (char i in txtSL.Text)
            {
                if (!char.IsDigit(i))
                {
                    errorProvider1.SetError(txtSL, "Vui lòng nhập số!");
                    return;
                }
            }
            errorProvider1.Clear();
            int SL = Convert.ToInt32(txtSL.Text);
            txtGia.Text = (SL * Convert.ToInt32(dr["dongia"].ToString())).ToString();
        }

        private void bntHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bntDongy_Click(object sender, EventArgs e)
        {
            foreach (char i in txtSL.Text)
            {
                if (!char.IsDigit(i))
                {
                    errorProvider1.SetError(txtSL, "Vui lòng nhập số!");
                    return;
                }
            }
            if (txtSL.Text == "0")
                this.Close();
            DataTable dtChitiet = SqlServerHelper.ExecuteDataTable(string.Format("select * from {0} where mahoadon = @mahoadon and mamon = @mamon", table_chitiethoadon), CommandType.Text, "@mahoadon", SqlDbType.Int, mahoadon, "@mamon",SqlDbType.Int, Convert.ToInt32(dr["mamon"]));
            if (dtChitiet.Rows.Count > 0)
            {
                string sql = string.Format("update {0} set soluong = soluong + @soluong where mamon = @mamon", table_chitiethoadon);
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@soluong", SqlDbType.Int, Convert.ToInt32(txtSL.Text),
                    "@mamon", SqlDbType.Int, Convert.ToInt32(dr["mamon"])
                    );
            }
            else
            {
                string sql = string.Format("insert into {0} (mahoadon,mamon,soluong,gia) values (@mahoadon,@mamon,@soluong,@gia)", table_chitiethoadon);
                SqlServerHelper.ExecuteNonQuery(sql, CommandType.Text,
                    "@mahoadon", SqlDbType.Int, mahoadon,
                    "@mamon", SqlDbType.Int, Convert.ToInt32(dr["mamon"].ToString()),
                    "@soluong", SqlDbType.Int, Convert.ToInt32(txtSL.Text),
                    "@gia", SqlDbType.Float, Convert.ToDouble(txtGia.Text)
                    );
            }
            this.Close();
        }
    }
}
